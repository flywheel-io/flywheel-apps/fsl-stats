"""Module to test main.py"""
from unittest.mock import MagicMock, patch

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_fslstats.main import run


@patch(
    "flywheel_gear_toolkit.GearToolkitContext",
    spec=True,
    config={"Mean intensity": True, "Stdev": True, "debug": False},
    config_json={"inputs": {"input_image": "test_image.nii.gz"}},
)
def test_run(mock_context):
    """
    Parser and command_line are tested separately. This test mocks
    the context and asserts that run will build the command, but not be able to
    run without a real image.
    """
    mock_context.config = {"Mean intensity": True, "Stdev": True, "debug": False}
    with pytest.raises(RuntimeError) as excinfo:
        run(mock_context)
    assert "following command has failed" in str(excinfo.value)
