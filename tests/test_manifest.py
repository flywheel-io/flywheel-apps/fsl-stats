import sys

from flywheel_gear_toolkit.utils.manifest import Manifest


def validate_manifest():
    """Validate Flywheel gear manifest"""
    manifest = Manifest("manifest.json")
    assert manifest.validate() is not None
