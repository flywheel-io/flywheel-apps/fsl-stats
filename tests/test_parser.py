"""Module to test parser.py"""
from unittest.mock import patch

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_fslstats.parser import parse_config


@patch(
    "flywheel_gear_toolkit.GearToolkitContext",
    spec=True,
    config={"Mean intensity": True, "Stdev": True, "debug": False},
    config_json={"inputs": {"input_image": "test_image.nii.gz"}},
)
class ProfileParser:
    def __init__(self):
        pass

    def test_parser_requires_args(self, mock_context):
        """
        Parser should exit if no statistical tests are requested.
        """
        sys_exit = pytest.raises(SystemExit)
        assert parse_config(mock_context) == sys_exit.type

    def test_parses_args_for_translation(self, mock_context):
        """
        Parser should handle debug field, which does not have a translation
        key in the manifest. Main purpose of the test is to show that the
        exception clause is accessible.
        """
        debug, gear_args, image_filepaths = parse_config(mock_context)
        assert debug == False
        assert gear_args is not []
        assert image_filepaths is not []

    def test_parses_debug_param(self, mock_context):
        mock_context.config = {"debug": True}
        with pytest.raises(KeyError) as pytest_parse_keyerror:
            parse_config(mock_context)
        assert pytest_parse_keyerror.type == KeyError

    def test_parser_calls_files(self, mock_context):
        """
        Test the image files are to be located.
        """
        mock_context.assert_called_with(parse_config._full_file_paths())
