## fsl-stats
The fsl-stats gear calculates user-specified statistics on an image using the algorithms 
in FSL's fslstats.

This gear translates human-readable options from Flywheel's GUI to commandline options 
used in fslstats.  

### Quick start
#### Inputs
- Images
    - An input image is required  
      This is the base image on which the stats are run and to which the mask images 
      may be applied.
    - Mask images (e.g., ROIs) or a secondary image to compare via a difference are 
      optional
      
  
- Statistical options
    - The options are the same as defined by fslstats proper. (See below for usages from FSL)
    - This implementation of fsl-stats draws from the UI configuration, available through 
      GearToolkit.context.config_json to determine
the user's arguments.
    - By default, mean (with and without zeroes) is selected.
    - Other options, which may be seen on an instance, correspond with the config parameter 
      in the manifest.json
    - Options are matched with their "id" variable to build a command for fslstats to 
      process
      
Note  
Functional images can be collapsed across timepoints or analyzed at each timepoint. To 
enable statistics at each timepoint, the "Split by timepoint" selection must be made.
  
### Usage from FSL
```
Usage: fslstats [preoptions] <input> [options]

preoption -t will give a separate output line for each 3D volume of a 4D timeseries
preoption -K < indexMask > will generate seperate n submasks from indexMask, for indexvalues 1..n where n is the maximum index value in indexMask, and generate statistics for each submask
Note - options are applied in order, e.g. -M -l 10 -M will report the non-zero mean, apply a threshold and then report the new nonzero mean

-l <lthresh> : set lower threshold
-u <uthresh> : set upper threshold
-r           : output <robust min intensity> <robust max intensity>
-R           : output <min intensity> <max intensity>
-e           : output mean entropy ; mean(-i*ln(i))
-E           : output mean entropy (of nonzero voxels)
-v           : output <voxels> <volume>
-V           : output <voxels> <volume> (for nonzero voxels)
-m           : output mean
-M           : output mean (for nonzero voxels)
-s           : output standard deviation
-S           : output standard deviation (for nonzero voxels)
-w           : output smallest ROI <xmin> <xsize> <ymin> <ysize> <zmin> <zsize> <tmin> <tsize> containing nonzero voxels
-x           : output co-ordinates of maximum voxel
-X           : output co-ordinates of minimum voxel
-c           : output centre-of-gravity (cog) in mm coordinates
-C           : output centre-of-gravity (cog) in voxel coordinates
-p <n>       : output nth percentile (n between 0 and 100)
-P <n>       : output nth percentile (for nonzero voxels)
-a           : use absolute values of all image intensities
-n           : treat NaN or Inf as zero for subsequent stats
-k <mask>    : use the specified image (filename) for masking - overrides lower and upper thresholds
-d <image>   : take the difference between the base image and the image specified here
-h <nbins>   : output a histogram (for the thresholded/masked voxels only) with nbins
-H <nbins> <min> <max>   : output a histogram (for the thresholded/masked voxels only) with nbins and histogram limits of min and max

Note - thresholds are not inclusive ie lthresh<allowed<uthresh
```
 
### Output
Currently, redirected to the log file.

## Contributing

For more information about how to get started contributing to that gear, 
checkout [CONTRIBUTING.md](CONTRIBUTING.md).
