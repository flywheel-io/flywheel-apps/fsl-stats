"""Main module for fsl-stats (commandline)."""

import logging
import os.path as op

from flywheel_gear_toolkit.interfaces import command_line
from flywheel_gear_toolkit.utils.manifest import Manifest

from fw_gear_fslstats import parser

log = logging.getLogger(__name__)


def run(gtk_context):
    """Gather the input arguments, parse,"""
    log.info("Begin parsing")
    # Prefix fslstats with "fsl5.0-" b/c it is sourced from neurodebian
    command = ["fsl5.0-fslstats"]
    # Parse the input
    debug, gear_args, image_filepaths = parser.parse_config(gtk_context)

    # Build the command
    if "t" in gear_args.keys():
        command.append("-t")
        del gear_args["t"]
    for image, filepath in image_filepaths.items():
        if image == "input_image":
            command.append(filepath)
        else:
            command.append("-" + image + " " + filepath)
    command = command_line.build_command_list(command, gear_args)

    try:
        # This command runs on FW instance and should be the first attempt, as
        # calls the fsl5.0 version from neurodebian specifically
        command_line.exec_command(command, shell=True)
    except RuntimeError:
        # This command version is the more natural, generic "fslstats", which
        # should run if a different base image is used or in some testing cases.
        command.pop(0)
        command.insert(0, "fslstats")
        command_line.exec_command(command, shell=True)
    return 0
