"""Parser module to parse gear config.json."""
import logging
import os
import os.path as op
from typing import Tuple

from flywheel_gear_toolkit import GearToolkitContext
from flywheel_gear_toolkit.utils.manifest import Manifest

log = logging.getLogger(__name__)
manifest_json = Manifest(op.join(op.dirname(op.dirname(__file__)), "manifest.json"))

# This function mainly parses gear_context's config.json file and returns relevant inputs and options.
def parse_config(gear_context: GearToolkitContext) -> Tuple[str, str]:
    """Harvest inputs from the user and translate into parsed arguments for run.py

    Returns:
        debug
        gear_args (list): translated list of characters to be passed to command_line.build_command_list
        image_filepaths (list): full filepaths to pass to FSL command
    """
    debug = gear_context.config.get("debug")
    gear_args = _match_fsl_commands(gear_context)
    image_filepaths = _full_file_paths(gear_context)

    return debug, gear_args, image_filepaths


def _match_fsl_commands(gear_context):
    """
    Collect the id keys from the manifest, if the argument was given through the UI.
    Args:
        gear_context (obj): contains the configuration from the user input
    Returns
        paramList (list): list of single characters reflecting the selected options
    Example:
        _match_fsl_commands(gear_context), where 'config' contains
            {'Max voxel coords': True, 'Mean intensity': True,
            'Mean intensity (nonzero)': True, 'ROI stats': True,
            'Split by timepoint': True}
        paramList = ['x','m','M','w','t']
    """
    args = list(gear_context.config.keys())
    if not args:
        log.error("fslstats requires at least one statistic be selected. Exiting.")
        os.sys.exit()

    params = {}
    for arg in args:
        try:
            # Complete the conversion from the UI context config to the manifest config id label
            params[manifest_json.config[arg]["id"]] = gear_context.config[arg]
        except KeyError:
            if arg == "debug":
                # debug is required by new GitLab templating, but is not parsed for fsl command
                pass
            else:
                log.error(
                    f"'id' needed to build argument for {arg}, but not found in {manifest_json.config}"
                )
                log.debug(f"Check toolkit version")
    return params


def _full_file_paths(gear_context):
    """FSL requires full file paths for input options, not relative paths. Supply the full file paths with this method
    Args:
        filenames (list): user inputs for the image files to be used
        For fslstats, only input_image, difference_image, and mask_image can be specified
    Returns:
        dict: key is the input value, value is the full file path
    """
    filepaths = {}
    env_path = manifest_json.environment["FLYWHEEL"]
    filetypes = gear_context.config_json["inputs"].keys()
    for ft in filetypes:
        if ft == "input_image":
            filepaths[ft] = op.join(env_path, ft, gear_context.get_input_path(ft))
        elif ft == "mask_image":
            filepaths["k"] = op.join(env_path, ft, gear_context.get_input_path(ft))
        else:
            filepaths["d"] = op.join(env_path, ft, gear_context.get_input_path(ft))
    return filepaths
