#!/usr/bin/env python3
"""The run script"""
import logging
import sys
from importlib import metadata

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_fslstats.main import run

log = logging.getLogger(__name__)
log.info(f"GearToolkit running version {metadata.version('flywheel_gear_toolkit')}")


def main(context: GearToolkitContext):  # pragma: no cover
    """Parses config and run"""
    e_code = run(context)

    # Exit the python script (and thus the container) with the exit
    # code returned by example_gear.main.run function.
    sys.exit(e_code)


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:
        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
